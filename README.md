# my-first-web-component

## Qué son los web components

Los web components son un conjunto de tecnologías HTML, CSS y JavaScript que buscan crear una forma estándar de creación de componentes, es decir, una forma de unir en un mismo marco HTML, CSS y JavaScript con la finalidad de resolver un problema desde un enfoque general y reutilizable.

Este conjunto de tecnologías son:

1. Custom Elements
2. Templates
3. Shadow DOM
4. Módulos ES (ESM)
5. CSS Scopes & Shadow Parts

### Web Component Vanilla

Un web component nativo extiende de HTMLElement y es entendido por el navegador haciendo uso de "define" donde asociamos el nombre la etiqueta con la clase que tiene el CSS, HTML y JavaScript asociados.

