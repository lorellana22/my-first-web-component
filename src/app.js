export class AppElement extends HTMLElement {
  //es imprescindible crear una función "constructor"
  constructor() {
    super(); //lo que hace "super" es llamar al constructor de la clase que esta por encima (HTMLElement)
    console.log("inicializo App Element");
    //this.innerHTML = "Hola mundo";//esto no es buena idea
  }

  //hay que usar esto:
  connectedCallback() {
    this.hello = this.getAttribute("hello") || "Hola Mundo"; //este ligh dom queda sobreescito por el shadow Dom
    this.innerHTML = `<p>${this.hello}</p>`; //esto lo visualiza un lector de pantalla o un sistema de arañas de Seo
    this.attachShadow({ mode: "open" }); //hemos abierto el shadow
    //escribimos en el propio ShadowDom, esto lo viasualiza nuestro ojo:
    this.shadowRoot.innerHTML = `
    <style>
        :host {
            display: block;
            border: black solid 2px;
        }
        p {
            color: var(--color-poc, green);
        }
        ::slotted(p) {
            color: red;
        }
    </style>
    <p>Hola shadow</p>
    <button>Click!</button>
    `;

    //el :host sirve para indicar que los estilos que contien aplique sobre la etiqueta "app-element",
    //no sobre el contenido definido dentro del template sino sobre la propia etiqueta
    //seria una forma de ver los limites que tiene nuestro web component dentro de la aplicacion


    const button = this.shadowRoot.querySelector("button");
    //como el shadow dom está abierto hay que hacer el querySelector a partir del "shadowRoot"
    button.onclick = (e) => this.clickMe(e);
  }


  clickMe(e) {
    console.log(e);
    const message = new CustomEvent("poc:message", {
        //propiedades:
      bubbles: true,
      composed: true, //cnd queremos que otro elemento dentro del shadowDom se entere de este evento
      cancelable: true, //indica si se puede hacer un preventDefault de este evento o no
      detail: {
        msg: "hola desde el componente",
      },
    });
    this.dispatchEvent(message);
  }
}
customElements.define("app-element", AppElement); //definie nuestro custom Element

//En caso de querer ejecutar algún tipo de código en el momento en el que nuestro web
//componente se carga en el DOM podemos hacer uso de "whenDefined" de esta forma:
customElements.whenDefined("app-element").then(() => {
  console.log("AppElement ha sido definido");
});
